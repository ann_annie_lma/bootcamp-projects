package com.example.firstproject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    lateinit var userInput:EditText
    lateinit var resultText:TextView
    lateinit var resultButton:Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()

        resultButton.setOnClickListener {

            val userInputNumber = userInput.text.toString().toIntOrNull()

            resultText.text =
                    userInputNumber?.let { it1 -> NumbersToWords().numbersToWords(it1).toString() }

            userInput.setText("")
        }
    }


    fun init()
    {
        userInput = findViewById(R.id.et_user_input)
        resultText = findViewById(R.id.tv_result)
        resultButton = findViewById(R.id.btn_result)
    }
}