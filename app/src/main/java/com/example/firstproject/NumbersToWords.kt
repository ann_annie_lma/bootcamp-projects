package com.example.firstproject

class NumbersToWords {

    val listOfTens = listOf<String>("","ათი","ოცი","ოცდაათი","ორმოცი","ორმოცდაათი","სამოცი",
    "სამოცდაათი","ოთხმოცი","ოთხმოცდაათი")

    val listOfHundreds = listOf("","ასი","ორასი","სამასი","ოთხასი","ხუთასი","ექვსასი","შვიდასი","რვაასი","ცხრაასი","ათასი")

    val numbers = listOf("","ერთი","ორი","სამი","ოთხი","ხუთი","ექვსი","შვიდი","რვა","ცხრა","ათი","თერთმეტი","თორმეტი","ცამეტი","თოთხმეტი",
    "თხუთმეტი","თექვსმეტი","ჩვიდმეტი","თვრამეტი","ცხრამეტი")


    //აქ რა მიწერია მე ვიცი და ღმერთმა,თუმცა მგონი მეც არ ვიცი უკვე :დდდდ

    fun numbersToWords(number:Int):String
    {
        var mutableNumber = number
        var current = ""

        if(number % 100 < 20)
        {
            current = numbers[number % 100]

            mutableNumber /= 100
        } else

        {

            if((mutableNumber / 10) % 2 == 0)
            {

                current = numbers[mutableNumber % 10]


                if(mutableNumber % 10 == 0)
                {

                    mutableNumber /= 10
                    current = listOfTens[mutableNumber % 10] + current
                }
                else
                {
                    mutableNumber /= 10
                    current = listOfTens[mutableNumber % 10].dropLast(1) + "და" + current
                }


            } else
            {
                current = numbers[mutableNumber % 10 + 10]
                mutableNumber /= 10
                current = listOfTens[mutableNumber % 10].dropLast(3) + current

            }

            mutableNumber /= 10
        }



        if(mutableNumber == 0)
            return current


        if(number % 100 == 0)
            return listOfHundreds[mutableNumber] + current;

        return listOfHundreds[mutableNumber].dropLast(1) + current;
    }


}

